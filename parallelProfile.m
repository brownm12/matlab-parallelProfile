function confName = parallelProfile
% System or other parameters
maxworkers = 224;               %Cluster size

% sys = [sysname '.arc.vt.edu'];  %Full system address
loginnode = 'calogin[1-2]|dtlogin[1-2]|brlogin[1-2]|nrlogin[1-8]';     %Regular expression for the login node names
sysOS = 'unix';

% Figure out the path on the cluster from the user's MATLAB version
v = ver('matlab');
v = v.Release(2:end-1);
v = strrep(v, ' Prerelease', ''); % Fix version name for prerelease versions of Matlab
mlpath = ['/opt/apps/matlab/' v];

fprintf('Initiating MATLAB version %s setup for submission to an ARC cluster...\n',v);

% We need to do different stuff for submission within the cluster (from a login node)
[~,hostname]=system('hostname'); hostname=strtrim(hostname);
oncluster = regexp(hostname,loginnode); % returns index of match if there is one, and [] otherwise
oncluster = ~isempty( oncluster ); % booleanize oncluster variable

%% If on the cluster, get the user ID
if ( oncluster )
  sysname = strrep(hostname, 'calogin', 'cascades');
  sysname = strrep(hostname, 'dtlogin', 'dragonstooth');
  sysname = strrep(hostname, 'brlogin', 'blueridge'); %% MAB replaced sysname with hostname as 1st arg
  sysname = strrep(hostname, 'nrlogin', 'newriver');  %% MAB replaced sysname with hostname as 1st arg

  %fprintf('Initiating MATLAB version %s setup for submission to the %s cluster...\n',v,sysname);

  fprintf('Detected that you are on login node (%s). Configuring for intracluster submission...\n',hostname);
  userid = getenv('USER');

%% If not on the cluster, prompt for cluster name and user ID
else
  % Prompt for cluster name
  fprintf('detected not on cluster, prompting for cluster name and ID\n');
  sysname='';
  while isempty(sysname)
    opt={'Cascades','DragonsTooth','NewRiver','BlueRidge'};
    choice = multi_option_input('Enter the ARC system to which you would like to submit:\n', opt);
    fprintf('Selection: %s\n\n',choice);
    switch choice
      case 'Cascades'
        sysname = 'cascades2';

      case 'DragonsTooth'
        sysname = 'dragonstooth1';

      case 'NewRiver'
        sysname = 'newriver2';

      case 'BlueRidge'
        sysname = 'blueridge2';

    end
  end
  
  % Prompt for User ID (usually PID)
  userid = input('Enter your ARC user ID: ', 's');
end

%%
sys = [sysname '.arc.vt.edu'];  %FQDN of cluster login host

% Prompt for system jobs directory
sysdir = input(['\nEnter the folder in your cluster home directory where you would like MATLAB \nto track your jobs. Make sure that this location exists, but is not a \nfolder that you use regularly.\n/home/' userid '/'], 's');
sysdir = ['/home/' userid '/' sysdir];
sysdir = regexprep(sysdir, '//', '/'); %Remove any accidental double slashes

confName = [regexprep(sysname,'[0-9]','') '_' v]; %Profile (configuration) name is SystemName_MatlabVersion, e.g. "newriver_R2016b"

%% If on the cluster, check to make sure the folder exists, then set some variables
if ( oncluster )
  while ~exist(sysdir,'dir') % MAB - this will usually be skipped since sysdir was entered above
    foldercreated = 0;
    yesno = input('Folder does not exist. Would you like to create it?\n', 's');
    if (yesno == 'y' || yesno == 'Y' || yesno == 'yes') % MAB - added 'yes' as a third option
      foldercreated = mkdir(sysdir);
      if (foldercreated)
        fprintf('Folder %s created.\n',sysdir);
      else
        fprintf('Failed to create folder: %s\n',sysdir);
      end
    end
    if (~foldercreated)
      sysdir = input(['Please enter another location:\n/home/' userid '/'], 's');
      sysdir = ['/home/' userid '/' sysdir];
      sysdir = regexprep(sysdir, '//', '/'); %Remove any accidental double slashes
    end
    
  end

  dataloc = sysdir;
  sharedfs = true;  %Shared file system
  isf  = @independentSubmitFcn;     %Independent Submit Function
  csf  = @communicatingSubmitFcn;   %Communicating Submit Function
  gjsf = @getJobStateFcn;           %Get Job State Function
  djf  = @deleteJobFcn;            %Destroy Job Function
  scriptsloc = [mlpath '/toolbox/local/arc'];

else
%% If not on the cluster, prompt for a local folder and make sure it exists, then set some variables
  % Prompt for local jobs directory
  dataloc = input('\nEnter the folder on your computer where you would like MATLAB \nto track your jobs. This should not be a folder that you use regularly.\n', 's');
  while ~exist(dataloc,'dir')
    dataloc = input('Folder does not exist. Please enter another location:\n', 's');
  end

  sharedfs = false; %Not shared file system
  isf  = {@independentSubmitFcn, sys, sysdir};    %Independent Submit Function
  csf  = {@communicatingSubmitFcn, sys, sysdir};  %Communicating Submit Function
  gjsf = @getJobStateFcn;                         %Get Job State Function
  djf  = @deleteJobFcn;                          %Destroy Job Function
  scriptsloc = [matlabroot '/toolbox/local'];
end


% Matlab's functionality changed in version 2012a, so we have to do
% different things depending on whether we're in that version or later or
% in 2011b or earlier
dcv = ver('distcomp');

if str2double(dcv.Version) >= 6.11  
%% We're working with 2017b or later

  clexists = 0;
  %If the configuration exists already
  try
    cluster = parcluster(confName); %This throws an error if the cluster doesn't exist, skipping over this try section
    clexists = 1;
%    choice = questdlg(['Profile ' confName ' already exists. Would you like to overwrite it, rename it, or cancel setup?'],'Profile Exists','Overwrite','Rename','Cancel','Cancel');
    choice = three_option_input(['Profile ' confName ' already exists. Would you like to overwrite it, rename it, or cancel setup?'],'Profile Exists','Overwrite','Rename','Cancel','Cancel');

    switch choice
  %     case 'Overwrite'

      case 'Rename'
        renamed = 0;
        while renamed == 0
          try
            nm = input('\nEnter name to be used for old profile: ','s');
            saveAsProfile(cluster,nm);
            renamed = 1;
            fprintf('Profile %s renamed to %s.\n',confName,nm);
          catch
          end
        end

      case 'Cancel'
        disp('Configuration setup cancelled.');
        return;
    end
  catch
  end

  if clexists
    cluster = parcluster(confName);
  else
    cluster = parallel.cluster.Generic('JobStorageLocation', dataloc);
  end
  set(cluster,'JobStorageLocation',dataloc);
  set(cluster,'NumWorkers',maxworkers);
  set(cluster, 'ClusterMatlabRoot', mlpath);
  set(cluster, 'OperatingSystem', sysOS);
  set(cluster, 'HasSharedFilesystem', sharedfs);
  set(cluster, 'IntegrationScriptsLocation', scriptsloc);
  if ( ~oncluster ) % MAB - 9/27/2018. oncluster is now a boolean, so this works again 
    fprintf('\n-- Detected not on an ARC cluster. Adding necessary parallel cluster additional properties --\n')
    pcap = cluster.AdditionalProperties;
    pcap.addprop('ClusterHost');
    pcap.ClusterHost = sys
    pcap.addprop('RemoteJobStorageLocation');
    pcap.RemoteJobStorageLocation = sysdir
  end

  %save the profile
  if clexists
    saveProfile(cluster);
  else
    saveAsProfile(cluster,confName);
  end
  fprintf('\nCreated cluster profile %s.\n',confName);

  %Function name to test if support files are in the right place
  if ( oncluster )
    fcn = 'communicatingSubmitFcn'; 
  else
    fcn = 'communicatingSubmitFcn'; 
  end

elseif str2double(dcv.Version) >= 6  
%% We're working with 2012a or later

  clexists = 0;
  %If the configuration exists already
  try
    cluster = parcluster(confName); %This throws an error if the cluster doesn't exist, skipping over this try section
    clexists = 1;
    choice = three_option_input(['Profile ' confName ' already exists. Would you like to overwrite it, rename it, or cancel setup?'],'Profile Exists','Overwrite','Rename','Cancel','Cancel');

    switch choice
  %     case 'Overwrite'

      case 'Rename'
        renamed = 0;
        while renamed == 0
          try
            nm = input('\nEnter name to be used for old profile: ','s');
            saveAsProfile(cluster,nm);
            renamed = 1;
            fprintf('Profile %s renamed to %s.\n',confName,nm);
          catch
          end
        end

      case 'Cancel'
        disp('Configuration setup cancelled.');
        return;
    end
  catch
  end

  if clexists
    cluster = parcluster(confName);
  else
    cluster = parallel.cluster.Generic('JobStorageLocation', dataloc);
  end
  set(cluster,'JobStorageLocation',dataloc);
  set(cluster,'NumWorkers',maxworkers);
  set(cluster, 'ClusterMatlabRoot', mlpath);
  set(cluster, 'OperatingSystem', sysOS);
  set(cluster, 'HasSharedFilesystem', sharedfs);
  set(cluster, 'IndependentSubmitFcn', isf);
  set(cluster, 'CommunicatingSubmitFcn', csf);
  set(cluster, 'GetJobStateFcn', gjsf);
  set(cluster, 'DeleteJobFcn', djf);

  %save the profile
  if clexists
    saveProfile(cluster);
  else
    saveAsProfile(cluster,confName);
  end
  fprintf('\nCreated cluster profile %s.\n',confName);

  %Function name to test if support files are in the right place
  if ( oncluster )
    fcn = 'communicatingSubmitFcn'; 
  else
    fcn = 'communicatingSubmitFcn'; 
  end

else 
%% We're working with version 2011b or earlier
  %If the configuration exists already
  try
    tmpconf = distcomp.configuration(confName); %This throws an error if the cluster doesn't exist, skipping over this try section
%    choice = questdlg(['Configuration ' confName ' already exists. Would you like to overwrite it, rename it, or cancel setup?'],'Configuration Exists','Overwrite','Rename','Cancel','Cancel');
    choice = three_option_input(['Profile ' confName ' already exists. Would you like to overwrite it, rename it, or cancel setup?'],'Profile Exists','Overwrite','Rename','Cancel','Cancel');

    switch choice
      case 'Overwrite'
        distcomp.configuration.deleteConfig(confName);

      case 'Rename'
        while tmpconf.Name == confName
          try
            nm = input('\nEnter name to be used for old configuration: ','s');
            tmpconf.Name = nm;
            tmpconf.save();
            tmpconf = distcomp.configuration(confName);
            fprintf('Configuration %s renamed to %s.\n',confName,tmpconf.Name);
          catch
          end
        end

      case 'Cancel'
        disp('Configuration setup cancelled.');
        return;
    end
  catch
  end
  
  %Get a scheduler
  sched = findResource('scheduler', 'type', 'generic');
  %Add the scheduler parameters
  set(sched, 'ClusterMatlabRoot', mlpath);
  set(sched, 'ClusterOsType', sysOS);
  set(sched,'ClusterSize',maxworkers);
  set(sched, 'DataLocation', dataloc);
  set(sched, 'HasSharedFilesystem', sharedfs);
  set(sched, 'SubmitFcn', isf);
  set(sched, 'ParallelSubmitFcn', csf);
  set(sched, 'GetJobStateFcn', gjsf);
  set(sched, 'DestroyJobFcn', djf);

  %Create the configuration from the scheduler
  tmpconf = distcomp.configuration.createNewFromScheduler(sched);
  conf = distcomp.configuration(tmpconf);

  %Add the parallel jobs info
  pj = struct('MinimumNumberOfWorkers',8,'MaximumNumberOfWorkers',16);
  conf.paralleljob.setFromEnabledStruct(pj);
  
  %Rename it and add a description
  conf.Name = confName;
  conf.Description = ['Configuration for ' v ' on ' sysname];
  conf.save();
  fprintf('\nCreated configuration %s.\n',confName);

  fcn = 'parallelSubmitFcn'; %Function name to test if support files are in the right place
end
  
%% Give them a warning if we have reason to think that their support files
%have not been put in the right place.
if strcmp(which(fcn),'')
  fprintf('\nWarning: %s not found in the Matlab path. ',fcn);
  if (oncluster)
    fprintf('Folders may be added to Matlab''s path using the addpath() and savepath() commands or with the MATLABPATH environment variable.\n');
  else
    fprintf('Please make \nsure that the support files for remote submission have been copied into: \n%s\n',fcn,fullfile(matlabroot,'toolbox','local'));
  end
end

end %end parallelProfile()


%% three_option_input() checks to see whether a display is available.
%if so, it does a three-option dialog box. if not, it does a text-based input.
function choice = three_option_input(prompt,title,opt1,opt2,opt3,def)

  %No display available, use text-based input
  if usejava('jvm') && ~feature('ShowFigureWindows')
    prompt = [prompt ' Type ''' opt1 ''', ''' opt2 ''', or ''' opt3 ''': '];
    choice = input(prompt, 's');

  %Display is available, do a dialog box
  else
%    choice = questdlg(['Profile ' confName ' already exists. Would you like to overwrite it, rename it, or cancel setup?'],'Profile Exists','Overwrite','Rename','Cancel','Cancel');
    choice = questdlg(prompt,title,opt1,opt2,opt3,def);
  end
end

%% multi_option_input()
function choice = multi_option_input(prompt, opt)
  for i=1:length(opt) 
    prompt = [prompt sprintf(' %2d: %s\n', i, opt{i})]; 
  end
  choice = input(prompt, 's');
  %convert to character array if user entered a number
  [cnum, status] = str2num(choice);
  if (status)
    choice = opt{cnum};
  end
end
