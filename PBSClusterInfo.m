classdef PBSClusterInfo <  handle ; % use hgsetget ??
%CLUSTERINFO ClusterInfo class a subclass of 'handle'
%
%   Copyright 2011 The MathWorks, Inc.
%   Winston Yu
%   $Revision$ $Date$
%
  methods (Static,Access='private') %  properties ??
     function gp = Group()
        gp = 'PBSClusterInfo';
     end
  end
%
  methods (Static)
     function clearClusterInfo()
        try
           rmpref(PBSClusterInfo.Group)%should this be PBSClusterInfo.Group
        catch E %#ok<NASGU>
        end
     end
%
     function setExtraParameter(pa)
        if nargin==0 || ischar(pa)==false
           error('distcomp:clusterinfo:InvalidType','Extra Parameter must be a character string.')
        end
        setpref(PBSClusterInfo.Group,'ExtraParameter',pa)
     end
     function pa = getExtraParameter()
        try
           val = getpref(PBSClusterInfo.Group,'ExtraParameter');
        catch E %#ok<NASGU>
           val = [];
        end
        pa = val;
     end
  end % methods (Static)
%  
end  % classdef